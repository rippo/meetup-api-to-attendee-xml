﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using MeetupApi.Model;

namespace MeetupApi.Provider
{
   class Xml
   {
      public XDocument Process(List<Result> rsvpList)
      {

         var doc = new XDocument();
         var root = new XElement("Attendees");

         foreach (var item in rsvpList)
         {
            var attendee = new XElement("Attendee");
            attendee.SetAttributeValue("id", Guid.NewGuid());
            attendee.Add(new XElement("Name", item.Member.Name));
            root.Add(attendee);
         }
         doc.Add(root);
         return doc;

      }

   }
}
