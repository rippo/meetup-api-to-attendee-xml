﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using MeetupApi.Model;
using MeetupApi.Provider;
using RestSharp;

namespace MeetupApi
{
   internal class Program
   {
      private static void Main(string[] args)
      {
         var meetupApiKey = ConfigurationManager.AppSettings["MeetupApiKey"];
         var meetupGroupName = ConfigurationManager.AppSettings["MeetupGroupName"];

         var eventUrl = ConfigurationManager.AppSettings["MeetupEventsUrl"];
         var rsvpUrl = ConfigurationManager.AppSettings["MeetupRsvpUrl"];

         var excludeList = new List<string>(ConfigurationManager.AppSettings["AttendeesExcludeList"].Split(','));

         var filename = ConfigurationManager.AppSettings["AttendeeLocation"];


         var api = new ApiCall(meetupApiKey);

         //Get upcoming events
         var request = new RestRequest {Resource = eventUrl};
         request.AddUrlSegment("groupname", meetupGroupName);
         var events = api.Execute<EventDto>(request);

         //Get RSVP list from first event
         request = new RestRequest { Resource = rsvpUrl };
         request.AddUrlSegment("eventid", events.Results.First().Id);
         var rsvpList = api.Execute<RsvpDto>(request);

         //create XML
         var xml = new Xml();
         var list = rsvpList.Results.Where(w => !excludeList.Contains(w.Member.Name)).ToList();
         var result = xml.Process(list);
         result.Save(filename);


         Console.WriteLine("Created {0} attendees, {1} excluded", list.Count, excludeList.Count);
         foreach (var attendee in rsvpList.Results.Where(w => !excludeList.Contains(w.Member.Name)).ToList())
         {
            Console.WriteLine("  - " + attendee.Member.Name);
         }
         Console.WriteLine("Saved to {0}", filename);
         Console.ReadKey();
      }
   }

}
