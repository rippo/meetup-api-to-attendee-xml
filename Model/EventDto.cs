﻿using System.Collections.Generic;

namespace MeetupApi.Model
{
   public class EventItem
   {
      public string Status { get; set; }
      public string Id { get; set; }
   }


   public class EventDto
   {
      public List<EventItem> Results { get; set; }
   }
}
